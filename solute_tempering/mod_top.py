class ModTop:
    def __init__(self, top, gamma):
        self.top = top
        self.gamma = gamma
    
    def process(self, molname, index):
        self.mod_bonded()
        self.mod_charges(molname)
        self.mod_epsilon()
        self.mod_pairtypes()
        self.mod_cmap()
        self.mod_explicit_bonded()
        self.save_mod('{}/rest-{}-{:.3f}-{}'.format(self.top.dir, index, self.gamma, self.top.top))

    def mod_charges(self, molname):
        """
        looks for a line in topology section 'atoms'
        and modifies it as needed
        :return: None
        """
        for section in self.top.list_sections('atoms'):
            if section in self.top.mols[molname]:
                for l in range(len(self.top.sections[section])):
                    line = self.top.sections[section][l]
                    lspl = line[:line.find(';')].split() if ';' in line else line.split()
                    if len(lspl) > 7 and lspl[0][0] not in ["[", ";"] and lspl[1] in self.top.typelist \
                            and int(lspl[2]) in self.top.range:
                        modline = self.mod_charge_line(lspl)
                        self.top.sections[section][l] = modline

    def mod_charge_line(self, cont_list):
        """
        takes a line from the topology section 'atoms'
        and increases charge by self.dpar;
        also changes atomtype to modified (Y-prefixed)
        :param cont_list: line passed as a list of non-whitespace strings
        :return: an assembled line with type and/or charge modified
        """
        if len(cont_list) == 8:
            cont_list[6] = self.gamma ** 0.5 * float(cont_list[6])
            cont_list[1] = 'Y' + cont_list[1]
            fstring = '{:>6s}{:>11s}{:>7s}{:>7s}{:>7s}{:>7s}{:>11.4f}{:>11s}\n'
            return fstring.format(*cont_list)
        elif len(cont_list) == 11:
            cont_list[6] = self.gamma ** 0.5 * float(cont_list[6])
            cont_list[9] = self.gamma ** 0.5 * float(cont_list[9])
            cont_list[1] = 'Y' + cont_list[1]
            fstring = '{:>6s}{:>11s}{:>7s}{:>7s}{:>7s}{:>7s}{:>11.4f}{:>11s}{:>11s}{:>11.4f}{:>11s}\n'
            return fstring.format(*cont_list)
        else:
            return ' '.join(cont_list)

    def mod_epsilon(self):
        """
        looks for a line in topology section 'atomtypes'
        and modifies it as needed
        :return: None
        """
        section = self.top.list_sections('atomtypes')[0]
        for l in range(len(self.top.sections[section])):
            lspl = self.top.sections[section][l].split()
            if len(lspl) > 6 and lspl[0].startswith('Y') and lspl[0][1:] in self.top.typelist:
                modline = self.mod_eps_line(lspl)
                self.top.sections[section][l] = modline

    def mod_eps_line(self, cont_list):
        """
        takes a line from the topology section 'atomtypes'
        and increases sigma by self.dpar
        :param cont_list: line passed as a list of non-whitespace strings
        :return: an assembled line with sigma modified
        """
        cont_list[6] = float(cont_list[6]) * self.gamma
        fstring = '{:<11s}{:>3s}{:>11s}{:>10s}{:>3s}{:>20s}{:>15.5f}\n'
        return fstring.format(*cont_list[:7])

    def mod_bonded(self):
        for section in self.top.list_sections('bondtypes') + self.top.list_sections('angletypes') \
                + self.top.list_sections('dihedraltypes'):
            for l in range(len(self.top.sections[section])):
                lspl = self.top.sections[section][l].split()
                if len(lspl) > 4 and lspl[0][0] not in ['[', ';', '#'] and any([x in lspl for x in '12495']):
                    indices = []
                    urey_bradley = False
                    if '1' in lspl[:4]:
                        indices.append(lspl.index('1'))
                    if '2' in lspl[:5]:
                        indices.append(lspl.index('2'))
                    if '4' in lspl[:5]:
                        indices.append(lspl.index('4'))
                    if '9' in lspl[:5]:
                        indices.append(lspl.index('9'))
                    if '5' in lspl[:4]:
                        indices.append(lspl.index('5'))
                        urey_bradley = True
                    sep = min(indices)
                    # TODO also allow for fractional scaling, but avoid selecting wildcards
                    if all([a[0] in 'XY' for a in lspl[:sep]]):
                        modline = self.mod_bond_line(lspl, sep, urey_bradley)
                        self.top.sections[section][l] = modline
        self.sort_dihedrals()
    
    def sort_dihedrals(self):
        section = self.top.list_sections('dihedraltypes')[0]
        self.top.sections[section].sort(key=self.sorting_fn)
        # also need to sort impropers to have wildcards at the end
        try:
            section = self.top.list_sections('dihedraltypes')[1]
            self.top.sections[section].sort(key=self.sorting_fn)
        except IndexError:
            pass

    def mod_explicit_bonded(self):
        nf = {'bonds': 2, 'angles': 3, 'dihedrals':4}
        for sname in nf.keys():
            for sect_id in self.top.list_sections(sname):
                if sect_id in self.top.mols[self.top.molname]:
                    section = self.top.sections[sect_id]
                    modline = {}
                    for n, l in enumerate(section):
                        lspl = l[:l.find(';')].split() if ';' in l else l.split()
                        if len(lspl) > nf[sname]+1 and lspl[0][0] not in ['[', ';', '#'] and lspl[nf[sname]] in '1459' \
                                and all([int(lspl[i]) in self.top.modatoms for i in range(nf[sname])]):
                            modline[n] = self.mod_explicit_line(lspl, sname)
                    for n in modline.keys():
                        section[n] = modline[n]

    def mod_explicit_line(self, lspl, sect_type):
        if sect_type == 'bonds':
            if len(lspl) == 5:
                format_line = '{:>5s} {:>5s}    {:1s} {:12s} {:12.3f} ; scaled\n'
                lspl[4] = float(lspl[4]) * self.gamma
            elif len(lspl) == 7:
                format_line = '{:>5s} {:>5s}    {:1s} {:12s} {:12.3f} {:12s} {:12.3f} ; scaled\n'
                lspl[4] = float(lspl[4]) * self.gamma
                lspl[6] = float(lspl[6]) * self.gamma
            else:
                print("Could not modify line {}, check if it is correct".format(' '.join(lspl)))
                return ' '.join(lspl)
        elif sect_type == 'angles':
            if len(lspl) == 6:
                format_line = '{:>5s} {:>5s} {:>5s}    {:1s} {:12s} {:12.3f} ; scaled\n'
                lspl[5] = float(lspl[5]) * self.gamma
            elif len(lspl) == 8:
                format_line = '{:>5s} {:>5s} {:>5s}    {:1s} {:12s} {:12.3f} {:12s} {:12.3f} ; scaled\n'
                lspl[5] = float(lspl[5]) * self.gamma
                lspl[7] = float(lspl[7]) * self.gamma
            elif len(lspl) == 12:
                format_line = '{:>5s} {:>5s} {:>5s}    {:1s} {:12s} {:12.3f} {:12s} {:12.3f} {:12s} {:12.3f} {:12s} ' \
                              '{:12.3f} ; scaled\n'
                lspl[5] = float(lspl[5]) * self.gamma
                lspl[7] = float(lspl[7]) * self.gamma
                lspl[9] = float(lspl[9]) * self.gamma
                lspl[11] = float(lspl[11]) * self.gamma
            else:
                print("Could not modify line {}, check if it is correct".format(' '.join(lspl)))
                return ' '.join(lspl)
        elif sect_type == 'dihedrals':
            if len(lspl) == 8:
                format_line = '{:>5s} {:>5s} {:>5s} {:>5s}    {:1s} {:12s} {:12.7f} {:8s} ; scaled\n'
                lspl[6] = float(lspl[6]) * self.gamma
            elif len(lspl) == 11:
                format_line = '{:>5s} {:>5s} {:>5s} {:>5s}    {:1s} {:12s} {:12.7f} {:8s} {:12s} {:12.7f} {:8s} ; scaled\n'
                lspl[6] = float(lspl[6]) * self.gamma
                lspl[9] = float(lspl[9]) * self.gamma
            else:
                print("Could not modify line {}, check if it is correct".format(' '.join(lspl)))
                if '\n' not in ' '.join(lspl):
                    return ' '.join(lspl) + '\n'
                else:
                    return ' '.join(lspl)
        else:
            raise RuntimeError("bonded params {} not recognized".format(sect_type))
        return format_line.format(*lspl)

    @staticmethod
    def sorting_fn(line):
        """
        ensures that (1) multiple component dihedrals are placed in consecutive
        lines and (2) wildcards are placed at the end (otherwise are ignored)
        :param line: str, line to be assessed for sorting
        :return: int, sorting priority
        """
        lspl = line.split()
        if len(lspl) > 4 and lspl[0][0] not in ';[':
            value = sum(map(ord, lspl[0])) + 10**3*sum(map(ord, lspl[1])) \
                   + 10**6*sum(map(ord, lspl[2])) + 10**9*sum(map(ord, lspl[3]))
            if "X" not in lspl[:4]:
                return value
            else:
                return value + 10**12
        elif len(lspl) > 2 and lspl[0][0] == '[':
            return -1
        else:
            return 0

    def mod_bond_line(self, cont_list, sep, ub):
        """
        takes a line from the topology section 'atomtypes'
        and increases sigma by self.dpar
        :param cont_list: list, line passed as a list of non-whitespace strings
        :param sep: int, position of the separator (interaction type number)
        :param ub: bool, whether an Urey-Bradley term
        :return: str, an assembled line with sigma modified
        """
        if ub:
            cont_list[sep + 4] = '{:.4f}'.format(float(cont_list[sep + 4]) * self.gamma)
        cont_list[sep + 2] = '{:.4f}'.format(float(cont_list[sep + 2]) * self.gamma)
        fstring = '{:>8}' * (sep+1) + '{:>20}' * (len(cont_list)-(sep+1)) + '\n'
        return fstring.format(*cont_list)

    def mod_pairtypes(self):
        if {'pairtypes', 'nonbond_params'} & set(self.top.section_headers):
            for section in self.top.list_sections('pairtypes') + self.top.list_sections('nonbond_params'):
                for l in range(len(self.top.sections[section])):
                    lspl = self.top.sections[section][l].split()
                    if len(lspl) > 4 and lspl[0][0] not in ['[', ';']:
                        if any([a.startswith('Y') for a in lspl[:2]]):
                            modline = self.mod_pt_line(lspl)
                            self.top.sections[section][l] = modline

    def mod_pt_line(self, cont_list):
        """
        takes a line from the topology section 'atomtypes'
        and increases sigma by self.dpar
        :param cont_list: line passed as a list of non-whitespace strings
        :return: an assembled line with sigma modified
        """
        count = sum([1 for x in cont_list[:2] if x.startswith('Y')])
        cont_list[4] = '{:.4f}'.format(float(cont_list[4]) * self.gamma ** (0.5 * count))
        fstring = '{:>8}{:>8}{:>8}{:>20}{:>20}\n'
        return fstring.format(*cont_list)
    
    def mod_cmap(self):
        if 'cmaptypes' in self.top.section_headers:
            modding = False
            section = self.top.list_sections('cmaptypes')[0]
            for l in range(len(self.top.sections[section])):
                lspl = self.top.sections[section][l].strip().split()
                if '1' in lspl and all([x.startswith('Y') for x in lspl[:5]]):
                    modding = True
                if '1' in lspl and not all([x.startswith('Y') for x in lspl[:5]]):
                    modding = False
                if len(lspl) > 0 and lspl[0][0] not in ['[', ';'] and '1' not in lspl and modding:
                    if lspl[-1].endswith('\\'):
                        lspl[-1] = lspl[-1].strip('\\')
                        line = " ".join(["{:.8f}".format(float(x) * self.gamma) for x in lspl]) + '\\'
                    else:
                        line = " ".join(["{:.8f}".format(float(x) * self.gamma) for x in lspl])
                    self.top.sections[section][l] = line + '\n'
    
    def save_mod(self, outname):
        with open(outname, 'w') as outfile:
            for section in self.top.sections:
                for line in section:
                    outfile.write(line)
                outfile.write('\n')
