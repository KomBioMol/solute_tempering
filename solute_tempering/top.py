import os
import warnings
from itertools import combinations


class Top:
    def __init__(self, filename, gmx_dir='/usr/share/gromacs/top/', rng=None):
        self.fname = filename  # full path to top file
        self.range = Toprange(rng)
        self.top = self.fname.split('/')[-1]
        self.dir = '/' + '/'.join(self.fname.split('/')[:-1])
        self.contents = open(self.fname).readlines()
        self.gromacs_dir = gmx_dir
        self.pref = ''
        self.ignored_ifs = []
        self.include_all()
        self.section_headers = [line.split()[1] for line in self.contents if line.startswith('[')]
        self.sections = self.get_sections()
        self.mols = self.def_molecules()
        self.typelist = None
        self.modatoms = None
        self.molname = None
    
    def process(self, molname):
        self.molname = molname
        self.merge_cmap()
        self.find_all_types(molname)
        for atomtype in self.typelist:
            self.clone_type(atomtype, 'Y', permutations=False if not self.range else True)
        self.assemble_cmap()
        self.remove_inverse_dih()
        
    def merge_cmap(self):
        if 'cmaptypes' in self.section_headers:
            cm = self.sections[self.list_sections('cmaptypes')[0]]
            new_cm = [cm[0]]
            new_line = []
            for line in cm[1:]:
                line = line.strip()
                new_line.extend(line.rstrip('\\').split())
                if not line.endswith("\\"):
                    new_line = " ".join(new_line) + '\n'
                    new_cm.append(new_line)
                    new_line = []
            self.sections[self.list_sections('cmaptypes')[0]] = new_cm
    
    def assemble_cmap(self):
        if 'cmaptypes' in self.section_headers:
            cm = self.sections[self.list_sections('cmaptypes')[0]]
            new_cm = [cm[0]]
            for line in cm[1:]:
                if len(line.split()) < 5 or line.startswith(';'):
                    new_cm.append(line)
                else:
                    lspl = line.split()
                    new_cm.append(" ".join(lspl[:8]) + '\\\n')
                    new_cm.extend([" ".join(lspl[8+10*x:8+10*(x+1)]) + '\\\n'
                                   for x in range(int(len(lspl[8:])/10))])
                    new_cm.append(" ".join(lspl[8+10*(len(lspl[8:])//10):]) + '\n')
            self.sections[self.list_sections('cmaptypes')[0]] = new_cm
        
    def find_all_types(self, molname):
        s = self.sections[self.mols[molname][0]]
        types = [l.split()[1] for l in s if len(l.split()) > 7 and l.strip()[0] not in [';', '[']
                 and int(l.split()[2]) in self.range]
        self.modatoms = {int(l.split()[0]) for l in s if len(l.split()) > 7 and l.strip()[0] not in [';', '[']
                         and int(l.split()[2]) in self.range}
        types = list(set(types))
        self.typelist = types
    
    def get_section(self, ptype):
        order = {'m': 't', 't': 'b', 'b': 'p', 'p': 'a', 'a': 'd', 'd': 'i'}
        if ptype in 'mtbpad':
            return [l for l in self.contents[self.get_bos(ptype):self.get_bos(order[ptype])] if not l.isspace()]
        elif ptype == 'i':
            return [line for line in self.contents[self.get_bos(ptype):] if line.startswith('[ dih')
                    or line.startswith(';  ai') or (len(line.split()) > 4 and line.split()[4] == '4')]
        elif ptype == 'h':
            return self.contents[:self.get_bos('m')]
        elif ptype == 'f':
            term_line = self.contents.index(self.get_section('i')[-1]) + 1
            return self.contents[term_line:]
    
    def get_bos(self, ptype):  # finds line num that starts respective section
        lines = [i for i in range(len(self.contents)) if len(self.contents[i].split()) > 2
                 and self.contents[i].split()[1] == self.sections[ptype]]
        if ptype in 'mtbpad':
            return lines[0]
        elif ptype == 'i':
            return lines[1]
    
    def include_all(self):
        next_include = self.find_next_include()
        while next_include:
            to_include = self.find_in_path(self.contents.pop(next_include).split()[1].strip('"\''))
            contents = open(to_include).readlines()
            self.contents[next_include:next_include] = contents
            next_include = self.find_next_include()
    
    def find_next_include(self):
        ifcount = 0
        for linenumber, line in enumerate(self.contents):
            if line.startswith("#if"):
                ifcount += 1
            elif line.startswith("#endif"):
                ifcount -= 1
            if line.startswith("#include") and ifcount == 0:
                return linenumber
            elif line.startswith("#include") and ifcount > 0 and line not in self.ignored_ifs:
                print("\n\nWarning: file {} will not be included and modified due to a dependency on .mdp settings -- "
                      "check for #ifdef keywords in your force field files; this is not problematic as long as the file"
                      " does not contain force field parameters.\n\n".format(line.split()[1].strip('"\'')))
                self.ignored_ifs.append(line)
        return None
    
    def find_in_path(self, filename):
        if filename in os.listdir(self.dir):
            return self.dir + '/' + filename
        else:
            if '/' in filename:
                self.pref = '/'.join(filename.split('/')[:-1])
            suff = filename.split('/')[-1]
            if self.pref.startswith('/') or self.pref.startswith('./'):
                first = self.pref.split('/')[1]
            else:
                first = self.pref.split('/')[0]
            if first in os.listdir(self.dir) and suff in os.listdir(self.dir + '/' + self.pref):
                return self.dir + '/' + self.pref + '/' + suff
            elif suff in os.listdir(self.gromacs_dir + '/' + self.pref):
                return self.gromacs_dir + '/' + self.pref + '/' + suff
        raise ValueError('file {} not found in neither local nor Gromacs directory'.format(filename))
    
    def get_sections(self):
        sections = []
        section_startlines = [0]
        headers_stack = self.section_headers[:]
        for l in range(len(self.contents)):
            line = self.contents[l]
            if headers_stack and headers_stack[0] in line and line.startswith('['):
                section_startlines.append(l)
                headers_stack.pop(0)
        section_startlines.append(len(self.contents))
        self.section_headers.insert(0, 'header')
        for s in range(len(section_startlines) - 1):
            sections.append(self.contents[section_startlines[s]:section_startlines[s + 1]])
        return sections
    
    def list_sections(self, section_name):
        return [i for i in range(len(self.section_headers)) if self.section_headers[i] == section_name]
    
    def def_molecules(self):
        """
        Extracts individual molecules from topology, creating a dict that binds
        the moleculename to a list of numbers of sections that contain its bonded params
        :return: dict containing molname:[section_numbers] bindings
        """
        mols = {}
        moltypes = self.list_sections('moleculetype')
        t, b, p, a, d = [self.list_sections(x) for x in ['atoms', 'bonds', 'pairs', 'angles', 'dihedrals']]
        bondeds = t + b + p + a + d
        moltypes.append(len(self.sections) - 1)
        for m, mnext in zip(moltypes[:-1], moltypes[1:]):
            sect = self.sections[m]
            molname = [x.split()[0] for x in sect if len(x.split()) > 1
                       and not x.lstrip().startswith('[') and not x.lstrip().startswith(';')][0]
            mols[molname] = [x for x in bondeds if m < x < mnext]
            mols[molname].sort()
        return mols
    
    def find_type(self, atomtype):
        sections = self.list_sections('atoms')
        results = []
        for s in sections:
            for l in range(len(self.sections[s])):
                lspl = l.split()
                if len(lspl) > 7 and lspl[1] == atomtype:
                    results.append('{}-{}-{}'.format(lspl[3], lspl[2], lspl[4]))
        return results

    def clone_type(self, atomtype, prefix, permutations=False):
        """
        adds modified (Y-prefixed) FF parameters if eps
        needs to be modified to ensure the modification only
        affects the specific subset of atoms
        :param atomtype: str, type to be cloned
        :param prefix: str, prefix to be added
        :return: None
        """
        param_sections = ['atomtypes', 'pairtypes', 'bondtypes', 'constrainttypes', 'angletypes',
                          'dihedraltypes', 'cmaptypes']
        to_check = [i for i in range(len(self.section_headers)) if
                    self.section_headers[i] in param_sections]
        for s in to_check:
            section = self.sections[s]
            for l in range(len(section)):
                line = section[l]
                newlines = []
                if len(line.split()) > 4 and not line[0] in ';[' \
                        and any(line.split()[i] == atomtype for i in range(5)):
                    newlines += self.gen_clones(line, atomtype, prefix, permutations)
                section.extend(newlines)
            self.sections[s] = section

    def gen_clones(self, line, atomtype, prefix, permutations=False):
        """
        copies the original parameters onto the modified type,
        also taking care of multiple modifications per line;
        the goal is to have both original terms,
        fully modified terms as well as all possible combinations
        of these; e.g. if we wish to clone "CA" in a line
        CA CA CT 1
        the result should be
        CA CA CT 1
        YCA CA CT 1
        CA YCA CT 1
        YCA YCA CT 1
        :param line: str, a line with parameter definition to be copied
        :param atomtype: str, type to be cloned
        :param prefix: str, prefix to be added
        :return: a list of modified lines
        """
        # TODO do not append originals
        lines = []
        endline = line.index(';') if ';' in line else len(line)-1
        nchanges = line[:endline].split().count(atomtype)
        if permutations:
            mods = []
            for i in range(nchanges):
                mods.extend(self.gen_combs(nchanges, i + 1))
        else:
            mods = [tuple(range(nchanges))]
        for m in mods:
            lines.append(self.mod_types(line, m, prefix, atomtype))
        return lines

    @staticmethod
    def mod_types(line, mods, prefix, atomtype):
        """
        takes a line and a tuple of numbers that specifies at which
        occurrences of the type the modification has to take place,
        and performs the requested modifications
        :param line: line to be modified
        :param mods: tuple of ints
        :param prefix: str, prefix to be added
        :param atomtype: str, type to be modified
        :return: modified line
        """
        for num in mods[::-1]:
            indices = [i for i in range(len(line) - len(atomtype) + 1)
                       if line[i:i + len(atomtype)] == atomtype
                       and (i == 0 or line[i-1].isspace())
                       and (i+len(atomtype) == len(line) or line[i+len(atomtype)].isspace())]
            line = line[:indices[num]] + prefix + line[indices[num]:]
        return line

    @staticmethod
    def gen_combs(count, tuples):
        """
        generates a list of possible modifications to be done on lines
        containing more than one occurrence of the type to be
        modified
        :param count: how many times the type to be changed occurs
        in the line
        :param tuples: how many modifications need to be performed
        :return: list of tuples containing occurrences to modify
        e.g. gen_combs(3, 2) denote three occurrences and two modifications
        -> [(0, 1), (0, 2), (1, 2)] are the possible combinations
        """
        combs = list(combinations(range(count), tuples))
        return combs
    
    def remove_inverse_dih(self):
        for dih_section in self.list_sections('dihedraltypes'):
            section = self.sections[dih_section]
            types = [(n, tuple(l.split()[:4])) for n, l in enumerate(section) if len(l.split()) >= 5
                     and not l.strip()[0] in ['#', ';']]
            t0, t1 = [x[0] for x in types], [x[1] for x in types]
            for i in range(len(types))[::-1]:
                if t1[i][::-1] in t1[:i]:
                    section.pop(t0[i])
            self.sections[dih_section] = section


class Toprange:
    def __init__(self, rn):
        if isinstance(rn, list):
            self.range = range(*rn)
        elif isinstance(rn, str):
            self.range = {int(x) for x in rn.split(',')}
        elif rn is None:
            self.range = range(10000000)
        self.bool = True if rn else False
        
    def __bool__(self):
        return self.bool
    
    def __contains__(self, item):
        return self.range.__contains__(item)
