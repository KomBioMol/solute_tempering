from optparse import OptionParser
import os, sys
from copy import deepcopy

if __name__ == "__main__":
    import top
    import mod_top
else:
    import solute_tempering.top as top
    import solute_tempering.mod_top as mod_top


class SolTemp:
    def __init__(self, opt):
        self.wdir = os.getcwd()
        self.top = self.abs_path(opt.topol)
        self.ff = opt.gmx_dir
        temperatures = [t for t in open(opt.t)]
        self.temperatures = [float(x) for x in temperatures[0].strip().split()]
        self.mol = opt.mol
        if opt.range and opt.reslist:
            raise RuntimeError('Please specify either -r or -l, not both')
        if opt.range:
            self.range = [int(x) for x in opt.range.split('-')]
        elif opt.reslist:
            self.range = opt.reslist
        else:
            self.range = None
        
    def abs_path(self, path):
        if path.startswith('/'):
            return path
        else:
            return self.wdir + '/' + path
    
    def mod_all(self):
        topol = top.Top(self.top, self.ff, self.range)
        topol.process(self.mol)
        for n, t in enumerate(self.temperatures):
            mod = mod_top.ModTop(deepcopy(topol), self.temperatures[0]/t)
            print("Now processing files for gamma = {:.3f}".format(mod.gamma))
            mod.process(self.mol, n)


def parse_all():
    parser = OptionParser(usage="")
    parser.add_option("-p", dest="topol", action="store", type="string",
                      help="Topology file")
    parser.add_option("-g", dest="gmx_dir", action="store", type="string", default="/usr/share/gromacs/top",
                      help="Path to GMX ff files")
    parser.add_option("-T", dest="t", action="store", type="string",
                      help="File with space-separated simulation temperatures, lowest (reference) should be first")
    parser.add_option("-m", dest="mol", action="store", type="string",
                      help="Molname of the molecule to change, as reported in the .top file")
    parser.add_option("-r", dest="range", action="store", type="string", default="",
                      help="Optional: residue range within the molecule specified with -m")
    parser.add_option("-l", dest="reslist", action="store", type="string", default="",
                      help="Optional: comma-separated list of residues, similar to -m (mutually exclusive)")
    (options, args) = parser.parse_args()
    options.kt = 0.008314
    return options


def main():
    opts = parse_all()
    if not opts.topol:
        print("No topology file was selected, exiting")
        sys.exit(1)
    s = SolTemp(opts)
    s.mod_all()


if __name__ == "__main__":
    main()
