from setuptools import setup

setup(name='solute_tempering',
      version='0.1',
      description='A script to generate Gromacs-compatible REST topologies',
      url='https://gitlab.com/KomBioMol/solute_tempering',
      author='Milosz Wieczor',
      author_email='milafternoon@gmail.com',
      license='GNU GPLv3',
      packages=['solute_tempering'],
      entry_points={'console_scripts': ['solute_tempering = solute_tempering.solute_tempering:main']},
      python_requires='>=3.4',
      zip_safe=False)
