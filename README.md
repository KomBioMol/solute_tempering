# Solute tempering

A Python tool to prepare topologies for replica exchange solute tempering
simulations (REST) in Gromacs

## Getting Started

The script follows the guidelines of the article by Terakawa et al.
(J Comput Chem 2011, "On easy implementation of a variant of the replica
exchange with solute tempering in GROMACS"). Please refer to it for
technical details.

To run the script, you need (1) a Gromacs topology file (.top) and
(2) a run control file that lists the desired simulation temperatures.
No additional libraries are required.

Run "python proton\_wire.py -h" to see available options.

## Usage

The general usage is as follows (brackets denote optional parameters):

```
python solute_tempering.py -p topol.top -T temperatures.dat -m solute_name [-g gromacs_topology_path]
```

### Specifying temperatures

The file temperatures.dat should list the temperatures of all replicas
in a single line, with the lowest (reference) listed as first, e.g.:

```
300.0 314.0 327.3 340.2
```

will result in 4 topology files generated with T=300 K for the reference replica.

### Specifying the solute molecule

The parameter solute_name should correspond to the molecule name in the
.top file (as listed in the \[ molecules \] and \[ moleculetype \] sections).
Currently only whole molecules can be selected for REST, but with minor
modifications it could be possible to perform REST for a part of
a molecule (or subset of molecules).

### Custom Gromacs install

The script includes all files to produce a single merged .top file; for
that purpose, it requires access to the Gromacs file library. By default,
Gromacs FF files are installed in /usr/share/gromacs/top, so if you have
Gromacs installed in a custom directory, provide the path as `-g`.

### Scaled interactions

The script performs scaling on:
+ charges
+ epsilons
+ bonded and plane angular terms (type 1)
+ Urey-Bradley terms (type 5)
+ CMAP terms
+ regular dihedrals (type 9)
+ improper dihedrals (type 2 and 4)